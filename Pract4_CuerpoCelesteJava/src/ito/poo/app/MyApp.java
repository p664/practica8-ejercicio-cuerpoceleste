package ito.poo.app;
import Pract4_CuerpoCelesteUML.CuerpoCeleste;
import Pract4_CuerpoCelesteUML.Ubicacion;

public class MyApp {

	public static void main(String[] args) {
		CuerpoCeleste c1=new CuerpoCeleste("Meteorito", "solido");
		CuerpoCeleste c2=new CuerpoCeleste("Estrella", "solido");
	    System.out.println(c1);
	    System.out.println(c2);
	    System.out.println(!c1.equals(c2));
	    System.out.println(c2.compareTo(c1));
	    
	    Ubicacion u1=new Ubicacion(94.05f, 239.47f, "8 meses", 940.10f);
	    Ubicacion u2=new Ubicacion(94.05f, 239.47f, "3 meses", 940.10f);
	    System.out.println(u1); 
	    System.out.println(u2); 
	    System.out.println(!u1.equals(u2));
	    System.out.println(u2.compareTo(u1)); 
	}
}